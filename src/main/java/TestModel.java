public class TestModel {

    public String testId;
    public String testName;

    public TestModel(String testId, String testName) {
        this.testId = testId;
        this.testName = testName;
    }

    public boolean isValid() {
        return !testId.equals(testName);
    }
}