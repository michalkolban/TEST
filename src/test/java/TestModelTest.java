import org.junit.Assert;
import org.junit.Test;

public class TestModelTest {


    @Test

    public void testWhenModeIsValid() {

        //given

        TestModel testModel = new TestModel("test", "test");

        //when

        boolean isValid = testModel.isValid();

        //then

        Assert.assertFalse(isValid);
    }
}